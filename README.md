# Javier Garcia(He/him/his)

I'm currently a member of the Digital Experience team at GitLab. I work primarily out of the ~~[Buyer Experience repo](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience)~~ [About GitLab repo](https://gitlab.com/gitlab-com/marketing/digital-experience/about-gitlab-com).

* San Diego, CA 🇲🇽
* BS in Graphic Communication, Computer Science Minor @ Cal Poly San Luis Obispo 
* MAS [Data Science & Enginering ~~student~~ graduate @ UC San Diego](https://gitlab.com/jgarc/mas-data-science-and-engineering) 🐍
* I have worked as a frontend developer for marketing teams [professionally](https://resume.javiergarcia.io/) since 2017. 
* I love open source software ❤️

Myers–Briggs Type Indicator: INFP

Clifton StrengthsFinder Top 5 
1. Strategic
2. Consistency
3. Restorative
4. Ideation
5. Intellection

## READMEs

* [Work From Home Setup](https://gitlab.com/jgarc/jgarc/-/blob/main/WORK-FROM-HOME-SETUP.md)
* [Developer Setup](https://gitlab.com/jgarc/jgarc/-/blob/main/DEVELOPER-SETUP.md)

