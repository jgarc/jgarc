# DEV SETUP 

## Speedy Mac Setup – Get up and running in a pinch.
1. Download Homebrew with the one line `curl` on homepage: https://brew.sh/
   1. You will need the Xcode developer tools for this 
1. Don't download `yarn`, `nodejs`, or `ruby` on its own; Do so with `asdf`: https://asdf-vm.com/guide/getting-started.html#_1-install-dependencies
   1. Homebrew/zsh install `brew install asdf`
      1. `asdf plugin add ruby` 
      1. `asdf plugin add nodejs`
      1. `asdf plugin add yarn`
      1. Go to repo with existing `.tool-versions` file (such as: https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience) and run `asdf install` to download some versions onto your system
   1. Add entry to `.zshrc` according to docs. My current: `. /opt/homebrew/opt/asdf/libexec/asdf.sh`
   1. Create `.tool-versions` in `~` to set defaults for `node`, `ruby`, `yarn`
     1. `ruby system`, `yarn 1.x`, `nodejs > 13.0.0` 
   1. Test with `which ruby|node|yarn`: should output something like `/.asdf/shims/ruby|node|yarn`
1. A simple Git autocomplete setup: 
```console
echo 'autoload -Uz compinit && compinit' >> ~/.zshrc
source ~/.zshrc
```
4. Jupyter Notebooks via Anaconda
5. [Neovim Quickstart](https://github.com/nvim-lua/kickstart.nvim)

Download VSCode or similar text editor. VSCode is recommended for most setups unless having strong preferences. Sublime Text is another good, lightweight, and performant option. Webstorm if you're into the Jetbrains ecosystem. You can also use emacs or vim if you're comfortable with that.  

## A more detailed Mac Setup. 
TODO
