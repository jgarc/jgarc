## Work from Home Setup:

### Tech:
* GitLab owned 2021 Mac Pro M1 Max: I had a 2020 Mac Air 16GB RAM that I thought was more than enough for the job. If it were my own money, I would have stuck to that.  
* Macbook Pro Docking Station iVANKY: great to make sure I'm hitting good refresh rates on both of my monitors, but not necessary. 
* Main Keyboard: [Qwerty Keys QK65V2](https://www.qwertykeys.com/collections/live-sales)
  * Tri-mode PCB / POM Plate / [Boba U4T Thocky switches](https://ringerkeys.com/products/gazzew-boba-u4t-thocky-tactile-switches)
* Travel Keyboard: HHKB Professional Classic S keyboard: Good small keyboard for devs
* Monitors: I referenced [RTINGS](https://www.rtings.com/monitor) for their entensive review repository. I prefer [IPS](https://www.groovypost.com/explainer/what-is-an-ips-display-and-how-it-compares-to-other-types/) monitors, but this is up to your needs. 
  * GIGABYTE M32U: main monitor that works great for lots of diverse settings in media, gaming, work, etc. 
  * Dell S2722QC 27-inch: side monitor could function as a great office on its own. Has amazing colors and unbeatable text rendering. Insane for programming. 
* Razor Viper Ultimate: my old generic mouse died on me, and needed to be replaced. 
* Ikea Tertial Desk Lamp
* Sony A6100 / 18-50mm Sigma f/2.8 set at 35mm
  * Connected via an Elgato Camlink 

### Sound:

Besides maybe the microphone and headphone setup, everything else is **definitely overboard**. _I would absolutely recommend an XLR setup_ for folks dealing with lots of ambient noise issues. There will not be a better investment if that disturbs your current work calls. The headphone setup is nice, but you can avoid the DAC/AMP if you're using the GitLab Mac Pro unless you're a complete audiophile. 

* Wireless bluetooth devices for convinience. Great for flights. 
  * Sony WF-1000XM5
  * Sony WH-1000XM4 
* Sony SSCS5 Bookshelf speakers: great value, but there are other powered options that could be better that doesn't require a receiver: Edifier R1280T for example 
* Sony STRDH550 Receiver
  * power the speakers via speaker wire
  * $150 on Offerup
* BEHRINGER Audio Interface + Cheap Amazon XLR Microphone ~$110 with XLR wire 
  * Taken from Linus Tech Tips their most expensive setup listed: [video](https://www.youtube.com/watch?v=vIQfDFARBLQ)
* Sennheiser x Massdrop HD 6XX: Great value open ear headphones. 
* Fulla Schitt DAC/AMP combo: powers wired headphones 

**Update**: I used ChatGPT to improve my audio setup. Here is the output of those prompts. I used two Amazon Basics 3.5mm Aux to 2 RCA Adapter Audio Cable to wire all of my devices together. _I would not recommend this setup for people starting from scratch._ I ended with this setup as a result of incremental improvements over time. If you have clear audio needs from the start, I would recommend consulting professionals or the audiophile subreddit for more efficient use of time and money. **I love my audio, and didn't want to start over so I went for the incremental improvements.** 

> The Sony STR-DH550 is a 5.2 channel A/V receiver that can work with many types of inputs, including HDMI, optical, and RCA. Here's how you can connect it to your Schiit Fulla 2 and Behringer U-Phoria UM2:

> 1. Behringer U-Phoria UM2: Connect the UM2 to your computer via USB for use as an audio interface. Your microphone plugs into the UM2.
> 2. Schiit Fulla 2: Connect the Fulla 2 to the UM2. You can do this by taking the RCA outputs from the UM2 and connecting them to the Fulla 2's input using an RCA to 3.5mm cable. You can then use the Fulla 2 as a headphone amplifier.
> 3. Sony STR-DH550: The receiver is designed to power your passive speakers, like the Sony SSCS5 you mentioned before. You can take the line out from your Schiit Fulla 2 (using the preamp output on the back) and connect it to one of the stereo analog inputs (using RCA cables) on the STR-DH550.

> So the signal flow would be: Computer (digital audio) -> UM2 (converts digital to analog) -> Fulla 2 (acts as a preamp and headphone amp) -> STR-DH550 (acts as a power amp for your speakers).

> Remember, when you want to listen through your headphones, use the headphone output on the Fulla 2. When you want to listen through your speakers, use the volume control on the STR-DH550. You may need to adjust the volume on the Fulla 2 and the UM2 as well to ensure a strong enough signal is reaching the STR-DH550, without distorting.

> In the context of your setup, you'd use the **Variable Preamp Output (4)** on the Fulla Schiit 2 to connect to your Sony STR-DH550 receiver. This output will allow you to use the volume knob on the Fulla 2 to control the level of the signal being sent to the receiver. This can be useful for fine-tuning the balance between your headphones (connected to the Fulla 2's headphone output) and your speakers (connected to the Sony receiver).

> You will need a 1/8" (3.5mm) TRS to dual RCA cable to make this connection. The 1/8" TRS end will plug into the Variable Preamp Output on the Fulla 2, and the dual RCA ends will plug into an analog input on your Sony receiver.

> Remember to select the correct input on your Sony receiver to match the RCA input you're using.

### Desk: 
* Home Depot: Unfinished Hevea Block Desktop Countertop: 30" x 60":
  * I could have gotten away with 24-30" x 48"
  * I wouldn't recommend for most people unless you're willing to do the upkeep required for a wood desk
     * Not an expert in this and there are plenty of better resources on the internet about oiling it every year or so
* HAIAOJIA Electric Stand up Desk Frame: Amazon standing desk legs
  * I did a DIY setup where I bought the legs, then put a preferred tabletop on it and screwed them in. Saved some money that way. 
* Power Strip velcrod to the bottom of the Desk for ease
* Ikea SIGNUMC able management, horizontal 
* Wrist Rests for keyboard and mouse
* Herman Miller Celle that I got from craiglist for $120 
* Desk mat for keyboard, mouse
* Amazon roller blade wheels
* Nice rug from Home depot to anchor the desk and chair 

### Personal gaming computer
Recently had on old gaming build die on me after [9 years of usage](https://pcpartpicker.com/user/javierg97/saved/sVJXsY). Thought I would include the new upgrade here: [PCPart Picker Completed Build](https://pcpartpicker.com/b/jFxcCJ#cx4256745)
